from app import app
from app.view.user import user
from app.view.admin import admin

app.register_blueprint(user)
app.register_blueprint(admin)

if __name__ == '__main__':
    app.run()
