from flask import Blueprint, request, jsonify
from app import login_manager
from app.models.common import Status
from app.models.admin import Admin
from flask_login import login_user

admin = Blueprint('admin', __name__,
                  url_prefix='/admin')


@admin.route("/login", methods=("POST",))
def admin_login():
    email = request.json.get('email')
    password = request.json.get('password')
    if not all((email, password)):
        success, other_details =  Status.ERROR, "Both Email and Password are required."
    try:
        email = email.lower()
        admin_obj = Admin.exists(email, password)
        if not admin_obj:
            success, other_details = Status.ERROR, "Wrong Credentails"
        else:
            success, other_details = Status.SUCCESS, admin_obj
    except Exception as err:
        success, other_details = Status.ERROR, str(err)

    if success:
        user = other_details
        login_user(user)
        user.update_last_login()
        return jsonify({
            "status": "success",
            "result": {
                "email": user.email,
                "first_name": user.first_name,
                "last_name": user.last_name
            }
        })
    else:
        return jsonify({
            "status": "error",
            "error": other_details
        }), 400
