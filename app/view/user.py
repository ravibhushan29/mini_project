from flask import Blueprint, request, jsonify
from app.models.common import Status
from app.models.user import User
from flask_login import login_user, current_user

user = Blueprint('user', __name__,
                 url_prefix='/user')


@user.route("/login", methods=("POST",))
def user_login():
    email = request.json.get('email')
    password = request.json.get('password')
    if not all((email, password)):
        return Status.ERROR, "Both Email and Password are required."
    try:
        email = email.lower()
        user_obj = User.exists(email, password)
        if not user_obj:
            success, other_details = Status.ERROR, "Wrong Credentails"
        else:
            success, other_details = Status.SUCCESS, user_obj
    except Exception as err:
        success, other_details = Status.ERROR, str(err)

    if success:
        user = other_details
        login_user(user)
        user.update_last_login()
        return jsonify({
            "status": "success",
            "result": {
                "email": user.email,
                "first_name": user.first_name,
                "last_name": user.last_name
            }
        })
    else:
        return jsonify({
            "status": "error",
            "error": other_details
        }), 400


@user.route("/register", methods=("POST",))
def user_register():
    email = request.json.get('email')
    password = request.json.get('password')
    first_name = request.json.get('first_name')
    last_name = request.json.get('last_name')

    if User.query.filter_by(email=email).first():
        success, other_details = Status.ERROR, "Email already exist"

    elif not all((email, password, first_name, last_name)):
        success, other_details = Status.ERROR, "Email, Password, first name and last name are required."
    else:
        success, other_details = Status.SUCCESS, None

    if success:
        user = User.register(first_name, last_name, email, password)
        return jsonify({
            "status": "success",
            "result": {
                "email": user.email,
                "first_name": user.first_name,
                "last_name": user.last_name
            }
        })
    else:
        return jsonify({
            "status": "error",
            "error": other_details
        }), 400


@user.route("/score", methods=("PATCH",))
def user_register():
    try:
        user = current_user
        user.score = request.json['score']
        return jsonify({
            "status": "success",
            "result": {
                "email": user.email,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "score": user.score
            }
        })
    except Exception as e:
        return jsonify({
            "status": "error",
            "error": str(e)
        }), 400
