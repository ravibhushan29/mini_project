from app import db
import datetime
import re
from sqlalchemy.ext.hybrid import hybrid_property
from app import bcrypt
from sqlalchemy.orm import validates


class Admin(db.Model):
    __tablename__ = 'admin'
    id = db.Column(db.Integer, primary_key=True)
    guid = db.Column(db.String, nullable=False, unique=True)
    email = db.Column(db.String, unique=True, nullable=False)
    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=True)
    password = db.Column(db.String, nullable=False)
    date_created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    last_login = db.Column(db.DateTime)

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def _set_password(self, password):
        self._password = bcrypt.generate_password_hash(password)

    @validates('email')
    def validate_email(self, key, data):
        if not re.match(r"[^@]+@[^@]+\.[^@]+", data):
            raise ValueError("Invalid Email Address")
        return data.lower()

    def check_passwd(self, passwd):
        return bcrypt.check_password_hash(self.password, passwd)

    def update_last_login(self):
        self.update(last_login=datetime.datetime.utcnow())