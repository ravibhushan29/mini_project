from app import db
import datetime
import re
from sqlalchemy.ext.hybrid import hybrid_property
from app import bcrypt
from sqlalchemy.orm import validates


class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    guid = db.Column(db.String, nullable=False, unique=True)
    email = db.Column(db.String, unique=True, nullable=False)
    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=True)
    password = db.Column(db.String, nullable=False)
    date_created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    last_login = db.Column(db.DateTime)
    score = db.Column(db.Integer)

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def _set_password(self, password):
        self._password = bcrypt.generate_password_hash(password)

    @validates('email')
    def validate_email(self, key, data):
        if not re.match(r"[^@]+@[^@]+\.[^@]+", data):
            raise ValueError("Invalid Email Address")
        return data.lower()

    def check_passwd(self, passwd):
        return bcrypt.check_password_hash(self.password, passwd)

    def update_last_login(self):
        self.update(last_login=datetime.datetime.utcnow())

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self

    @staticmethod
    def register(first_name, last_name, email, password):
        user = User(email=email, password=password, first_name=first_name, last_name=last_name)
        user.save()
        return user

    # def add_user(self):
    #     db.