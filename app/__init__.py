from flask import Flask

app = Flask(__name__)
app.config.from_object('app.config')
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt
from flask_login import LoginManager

db = SQLAlchemy()
migrate = Migrate(app, db)
bcrypt = Bcrypt()
login_manager = LoginManager()

